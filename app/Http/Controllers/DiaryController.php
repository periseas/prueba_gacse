<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DiaryController extends Controller
{
    public function index()
    {
        return view('admin.diary.index');
    }
    public function create()
    {
        return view('admin.diary.create');
    }
}

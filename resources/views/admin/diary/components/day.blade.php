<div class="row ">
    <div class="col-sm-6 col-md-6 dairy d-flex flex-column ">
        <h2>
            <span class="day-completed">
                <span class="day">19</span>, Agosto 2021
            </span>
        </h2>
        <h3>Jueves</h3> 
        @for ($i = 5; $i < 24; $i++)
            <div class="row diary-hour" 
                data="{{$i}}:00 - {{$i+1}}:00"
                @if($i==8)
                    doctor="Alfredo Lara Barba"
                    patient="Javier Sánchez Gutiérrez" 
                    years="31" 
                    email="lorem@insum.com" 
                    phone="222 2222 2222" 
                @endif
            >
                <div class="col-sm-3">
                    <p>{{$i}}:00 - {{$i+1}}:00</p> 
                </div>
                <div class="col-sm-9">
                    @for ($min = 0; $min < 2; $min++)
                        <p class="diary-hour-event"  data="{{$i}}:00 - {{$i+1}}:00">
                            @if($i==8)
                                @if($min==0)
                                    Medico: Alfredo Lara Barba
                                @else
                                    Paciente: Javier Sánchez Gutiérrez, Edad: 31 Años, Email: lorem@insum.com, Teléfono: 222 2222 2222 
                                @endif
                            @else
                                .
                            @endif
                        </p> 
                    @endfor
                </div>
            </div>
        @endfor
    </div> 
    <div class="col-sm-6 col-md-6 order-lg-last order-md-first order-sm-
    first">
        <div class="wrapper">
            <div class="fixed-header">
                <div class="box">
                    <div class="fb-xfbml-parse-ignore sticky">
                        @include ("admin.diary.components.miniCalendar")
                        @include ("admin.diary.components.event")
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

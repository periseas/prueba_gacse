<div class="row "> 
    <div class=" col-md-12 card"> 
        <form action="">
            <p class="h5">Datos de la cita</p>
            <div class="row">
                <div class=" col-md-6 "> 
                    <label for="">Nombre del medico: </label>
                    <input type="text" name="doctor_name" class="form-control">
                </div>
                <div class=" col-md-3 "> 
                    <label for="">Seleccionar día: </label>
                    <input type="date" name="date" class="form-control">
                </div>
                <div class=" col-md-3 "> 
                    <label for="">Horario disponible: </label>
                    <select name="horario" class="form-control">
                        @for ($i = 0; $i < 24; $i++)
                            <option value="{{$i}}:00-{{$i+1}}:00">{{$i}}:00 - {{$i+1}}:00</option>
                        @endfor
                    </select>
                </div>
            </div>
            <hr>
            <p class="h5">Datos del paciente</p>
            
            <div class="row">
                <div class=" col-md-6 "> 
                    <label for="">Nombre : </label>
                    <input type="text" name="patient_name" class="form-control">
                </div>
                <div class=" col-md-3 "> 
                    <label for="">Apellido paterno: </label>
                    <input type="text" name="patient_lastname" class="form-control">
                </div>
                <div class=" col-md-3 "> 
                    <label for="">Apellido materno: </label>
                    <input type="text" name="patient_m_lastname" class="form-control"> 
                </div>
            </div>
            <div class="row mt-1">
                <div class=" col-md-6 "> 
                    <label for="">Email: </label>
                    <input type="email" name="email" class="form-control">
                </div>
                <div class=" col-md-3 "> 
                    <label for="">Edad : </label>
                    <input type="number" name="years" class="form-control">
                </div>
                <div class=" col-md-3 "> 
                    <label for="">Teléfono: </label>
                    <input type="text" name="phone" class="form-control"> 
                </div>
            </div>
            <div class="row">
                <div class=" col-md-12 text-right"> 
                    <button class="btn btn-success m-1">Agendar cita </button>
                </div>
            </div>
        </form>
    </div>
</div>
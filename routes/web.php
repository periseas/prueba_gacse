<?php
use App\Http\Controllers\DiaryController;


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/* diary */
Route::group(
    [
        'prefix' => 'diary',
        'as' => 'diary.',
        'middleware' => ['web', 'auth'],
    ], 
    function () {
        Route::get('/', 'DiaryController@index')->name('index');
        Route::get('create', 'DiaryController@create')->name('create');
    }
);
 
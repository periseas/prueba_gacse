@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
                <h1 class="mt-0 pt-0"><small>Agenda medica </small></h1>
        </div>
    </div>
    
    <div class="clearfix"></div>

    @include('admin.diary.components.day')
</div> 

<script src="{{ asset('js/diary.js') }}"></script>

@endsection

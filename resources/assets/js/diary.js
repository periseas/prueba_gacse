$(document).ready(function(){
    $(".event").hide();

    var cite = clearCite();

    $(".diary-hour").hover(function()
        {
            $(this).addClass("active");
                     
            cite = {
                hour: $(this).attr("data"),
                day: $(".day-completed").html(),
                doctor: $(this).attr("doctor"),
                patient:  $(this).attr("patient"),
                years: $(this).attr("years"),
                email: $(this).attr("email"),
                phone: $(this).attr("phone"),
            };
            setCite(cite);

            $(".event").show();

        }, function(){
            $(this).removeClass("active"); 
            cite = clearCite();
            setCite(cite);
        }
    ); 

    $(".calendar-number").click(function()
        {
            var day = $(this).html();
            $(".day").html(day);
            $(this).removeClass("active"); 
            $(".event").hide();
        }
    ); 

    function setCite(cite){
        $(".event .hour").html(cite["hour"]);
        $(".event .event-day").html(cite["day"]);
        $(".event .doctor").html(cite["doctor"]);
        $(".event .patient").html(cite["patient"]);
        $(".event .years").html(cite["years"]);
        $(".event .email").html(cite["email"]);
        $(".event .phone").html(cite["phone"]);
    }

    function clearCite(){
        cite = {
            hour: "",
            day: "",
            doctor: "",
            patient: "",
            years: "",
            email: "",
            phone: "",
        };
        return cite;
    }
    
});
# Guía Medica 



## Tayde Areli Castilo Aguilar 
## Caracteristicas 

- Laravel 5.4
- Authentication de Laravel 
- Bootstrap 

## Instalación 
1. Clonar el repositorio con tus creenciales de gitlab, agrega nuevas llaves ssh si es necesario
1. usa **Git init**
1. usa **Git clone git@gitlab.com:periseas/prueba_gacse.git**
1. usa **php artisan key:generate** para crear tu llave de laravel
1. usa **composer install** 
1. usa **composer update** 
1. usa **crea el archivo .env cambiand el nombre de .env.example**
1. crea una base de datos  y asignala los datos en .env 
1. usa **php artisan migrate**  para correr las migraciones 

Dudas o comentario en **tayde.areli@gmail.com**

